import {useDispatch, useSelector} from "../../../redux/hook";

export function CounterContainer() {

    const count = useSelector(s => s.counter);
    const dispatch = useDispatch();

    function add() {
        dispatch({type: "INCREMENT"})
    }

    function minus() {
        dispatch({type: "DECREMENT"})
    }

    return <div>
        <h1>Counter</h1>
        <button onClick={minus}>-</button>
        <p>value: {count}</p>
        <button onClick={add}>+</button>
    </div>
}