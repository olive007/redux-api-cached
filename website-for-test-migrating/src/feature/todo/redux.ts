import {
    ReduxApiCachedActionMap,
    ReduxApiCachedActionCreator,
    ReduxApiCachedGeneratedState, generateActionAndReducerForObject
} from "redux-api-cached";


export interface Todo {
    id: number;
    userId: number;
    title: string;
    completed: boolean;
}

export type TodoReduxSubState = ReduxApiCachedGeneratedState<Todo>;

let actions: ReduxApiCachedActionMap;
let loadTodos: ReduxApiCachedActionCreator<void>;

export function register(moduleName: string = "todo") {

    [actions, loadTodos] = generateActionAndReducerForObject<Todo>(moduleName, "https://jsonplaceholder.typicode.com/todos", ["title"]);
}


export { actions as default, loadTodos }
