import React, {useEffect} from "react";

import {useDispatch, useSelector} from "../../../redux/hook";
import {loadTodos} from "../redux";

function TodoComponent({title = ""}) {

    return <li>{title}</li>
}

export default function TodoList() {

    const todos = useSelector(s => Object.values(s.todo.order.title.map((id) => s.todo.byId[id])))
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(loadTodos());
    }, [dispatch])

    return <>
        <h1>Todo</h1>
        <ul>
            {todos.map(t => <TodoComponent key={t.id} {...t}/>)}
        </ul>
    </>;
}