import {AnyAction} from "redux";

export default function tataReducer(state = [], action: AnyAction) {
    switch (action.type) {
        case 'ADD_TATA':
            // @ts-ignore
            return state.concat([action.text])
        default:
            return state
    }
}