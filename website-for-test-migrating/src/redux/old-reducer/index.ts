import { combineReducers } from "redux";

import tata from "./tata";
import counter from "./counter";

export const rootReducerMap = {
    tata,
    counter
};

const rootReducer = combineReducers(rootReducerMap);

export default rootReducer;
