import {ReduxApiCachedRootState} from "redux-api-cached/types";
import {reduxStoreFactory} from "redux-api-cached";

import {register as registerModuleTodo, TodoReduxSubState} from "../feature/todo/redux";
import {rootReducerMap} from "./old-reducer";

function registerFeatures() {
    registerModuleTodo();
}

export interface ReduxRootState extends ReduxApiCachedRootState {
    todo: TodoReduxSubState;
}

// @ts-ignore
const store = reduxStoreFactory(registerFeatures, rootReducerMap);

export default store