import {Action} from "redux";
import {DeepReadonly} from "deep-lock";


export interface ReduxApiCachedObjectWithId {
    id: string | number;
    [key: string|number]: any;
}

//////////////////////////////////////////////////////////////////////
// Redux State
//////////////////////////////////////////////////////////////////////

export interface ReduxApiCachedAbstractState {
    [key: string]: any;
}

export interface ReduxApiCachedSubState extends ReduxApiCachedAbstractState {
    [key: string | number]: any;
}

export interface ReduxApiCachedRootState extends ReduxApiCachedAbstractState {
    [key: string]: ReduxApiCachedSubState;
}

export interface ReduxApiCachedGeneratedState<DataType extends ReduxApiCachedObjectWithId>
    extends ReduxApiCachedAbstractState {

    byId: {
        [key: number]: DataType;
    }
    order: {
        [key: string]: DataType["id"][];
    };
}


//////////////////////////////////////////////////////////////////////
// Redux action
//////////////////////////////////////////////////////////////////////
export interface ReduxApiCachedAction<PayloadType = any> extends Action<string> {
    payload: PayloadType;
}

export type ReduxApiCachedActionCreator<PayloadType> =
    (payload: PayloadType) => ReduxApiCachedAction<PayloadType>;

export interface ReduxApiCachedActionMap {
    [key: string]: string;
}

//////////////////////////////////////////////////////////////////////
// Redux reducer
//////////////////////////////////////////////////////////////////////

export type ReduxApiCachedReducer<StateType extends ReduxApiCachedAbstractState, PayloadType> =
    (state: DeepReadonly<StateType>, payload: PayloadType) => StateType;

export interface ReduxApiCachedReducerMap<StateType extends ReduxApiCachedAbstractState> {
    [key: string]: ReduxApiCachedReducer<StateType, any>;
}
