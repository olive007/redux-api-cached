import mergeObjectsWithoutDuplicate from 'merge-objects-without-duplicate';
import {
    ReduxApiCachedAbstractState,
    ReduxApiCachedActionCreator,
    ReduxApiCachedActionMap,
    ReduxApiCachedReducer,
    ReduxApiCachedReducerMap,
    ReduxApiCachedRootState,
    ReduxApiCachedSubState
} from "./types";
import {Saga} from "redux-saga";
import deepLock, {DeepReadonly} from "deep-lock";

// TODO reformat
// This file allow developers to generate the 'action' function for redux.
// To generate an 'action' function just call the function 'generateActionFunc'
// the result will be its 'action' function. This guaranty their uniqueness.
// They can be dispatch by redux like any other hand-written 'action' functions.


interface MonoStateType {
    (): undefined;
    initialState: ReduxApiCachedRootState;
    reducers: ReduxApiCachedReducerMap<ReduxApiCachedRootState>;
    reducersByModule: {
        [key: string]: ReduxApiCachedReducerMap<ReduxApiCachedSubState>;
    };
    actionsByModule: {
        [key: string]: ReduxApiCachedActionMap;
    };
    sagaRunners: Saga<any[]>[];
}


const MonoState = function() {
    if (MonoState.initialState === undefined) {
        MonoState.initialState = {};
        MonoState.reducers = {};
        MonoState.reducersByModule = {};
        MonoState.actionsByModule = {};
        MonoState.sagaRunners = [];
    }
} as MonoStateType;


////////////////////////////////////////////////////////////////////////////////
// Getter
////////////////////////////////////////////////////////////////////////////////

/**
 * Get initial state
 */
export function getRegisteredInitialState() {
    MonoState();
    if (!Object.isFrozen(MonoState.initialState)) {
        // @ts-ignore
        MonoState.initialState = deepLock(MonoState.initialState);
    }
    return MonoState.initialState;
}

/**
 * Get the action type for a specific module.
 */
export function getRegisteredActionTypes(moduleName: string) {
    MonoState();
    if (!Object.isFrozen(MonoState.actionsByModule[moduleName])) {
        MonoState.actionsByModule[moduleName] = deepLock(MonoState.actionsByModule[moduleName]);
    }
    return MonoState.actionsByModule[moduleName];
}

/**
 * Get the reducer functions
 */
export function getAllRegisteredReducers(): ReduxApiCachedReducerMap<ReduxApiCachedRootState>  {
    MonoState();
    if (!Object.isFrozen(MonoState.reducers)) {
        MonoState.reducers = deepLock(MonoState.reducers);
    }
    return MonoState.reducers;
}

/**
 * Get the reducer functions
 */
export function getAllRegisteredReducersForModule(moduleName: string): ReduxApiCachedReducerMap<ReduxApiCachedSubState>  {
    MonoState();
    if (!Object.isFrozen(MonoState.reducersByModule)) {
        MonoState.reducersByModule = deepLock(MonoState.reducersByModule);
    }
    return MonoState.reducersByModule[moduleName];
}

/**
 * Get the reducer functions
 */
export function getAllSagaRunner() {
    MonoState();
    if (!Object.isFrozen(MonoState.sagaRunners)) {
        // @ts-ignore
        MonoState.sagaRunners = deepLock(MonoState.sagaRunners);
    }
    return MonoState.sagaRunners;
}

////////////////////////////////////////////////////////////////////////////////
// Method
////////////////////////////////////////////////////////////////////////////////

/**
 * Register a custom initial state to the main one.
 */
export function registerNewInitialState(moduleName: string, customInitialState: ReduxApiCachedSubState) {
    MonoState();
    MonoState.initialState = mergeObjectsWithoutDuplicate(MonoState.initialState, {[moduleName]: customInitialState});
}

/**
 * Register a reducer into the reduxStoreFactory and dynamically create the action type and function for the reducer
 *
 * @param {string}        moduleName  Name of the module
 * @param {string}        id          Id of the redux action
 * @param {Function|null} reducer     Redux reducer
 */
export function registerNewAction<StateType extends ReduxApiCachedSubState, PayloadType>(
    moduleName: string,
    id: string,
    reducer?: ReduxApiCachedReducer<StateType, PayloadType>) : ReduxApiCachedActionCreator<PayloadType>  {

    MonoState();

    // Define the action ID
    // Like we are sure that no action will be duplicated
    const actionId = (moduleName + "/" + id).toUpperCase();

    if (MonoState.reducers[actionId] === undefined) {
        if (MonoState.actionsByModule[moduleName] === undefined) {
            MonoState.actionsByModule[moduleName] = {};
            MonoState.reducersByModule[moduleName] = {};
        }
        if (MonoState.actionsByModule[moduleName][id] !== undefined) {
            throw new Error(`You are trying to overwrite an redux action type: '${id}'`);
        }
        MonoState.actionsByModule[moduleName][id] = actionId;

        if (reducer) {
            // @ts-ignore
            MonoState.reducersByModule[moduleName][actionId] = reducer;
            MonoState.reducers[actionId] = (state : DeepReadonly<{ [key: string]: ReduxApiCachedSubState }>, payload: PayloadType) => {
                return {
                    ...state,
                    // @ts-ignore
                    [moduleName]: reducer(state[moduleName], payload)
                };
            }
        }

    } else {
        // We are trying to override an existing action if the variable is defined
        throw new Error(`You are trying to overwrite an redux action: '${actionId}'`);
    }

    // Now we dynamically create the 'redux action' function.
    // The return value of this function will have to be dispatch by redux.
    // Redux will pass the argument from this generated function to the 'reducer' function
    return (payload) => {
        return {type: actionId, payload};
    }
}

/**
 * Register a saga watcher.
 */
export function registerNewSagaRunner(runner: Saga) {
    MonoState();
    MonoState.sagaRunners.push(runner);
}
