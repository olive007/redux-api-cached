import { takeEvery } from "@redux-saga/core/effects";
import {ReduxApiCachedActionCreator, ReduxApiCachedActionMap, ReduxApiCachedSubState} from "../types";
import {getRegisteredActionTypes, registerNewAction, registerNewInitialState, registerNewSagaRunner} from "../redux-helper";

type ErrorLevel = "WARM" | "FATAL";

export interface ErrorType {
    dateOccurred: Date;
    level: ErrorLevel;
    message: string;
    dateClosed?: Date;
}

export interface ErrorManagingReduxStateType extends ReduxApiCachedSubState {
    list: ErrorType[];
}

let warningErrorHappened: ReduxApiCachedActionCreator<Error>;
let fatalErrorHappened: ReduxApiCachedActionCreator<Error>;
let actions: ReduxApiCachedActionMap;

export function register(moduleName: string = "errorManager") {

    ////////////////////////////////////////////////////////////////////////////
    // Initial State
    ////////////////////////////////////////////////////////////////////////////

    registerNewInitialState(moduleName, {list:[]});

    ////////////////////////////////////////////////////////////////////////////
    // Actions and reducer
    ////////////////////////////////////////////////////////////////////////////

    warningErrorHappened = registerNewAction<ErrorManagingReduxStateType, Error>(moduleName, "NEW_WARM_ERROR", (state, payload) => {
        const newError: ErrorType = {
            dateOccurred: new Date(),
            level: "WARM",
            message: payload.message,
            dateClosed: undefined
        }
        return {
            ...state,
            list: [...state.list, newError]
        };
    });

    fatalErrorHappened = registerNewAction<ErrorManagingReduxStateType, Error>(moduleName, "NEW_FATAL_ERROR", (state, payload) => {
        const newError: ErrorType = {
            dateOccurred: new Date(),
            level: "FATAL",
            message: payload.message,
            dateClosed: undefined
        }
        return {
            ...state,
            list: [...state.list, newError]
        };
    });

    actions = getRegisteredActionTypes(moduleName);

    ////////////////////////////////////////////////////////////////////////////
    // Saga
    ////////////////////////////////////////////////////////////////////////////

    function logger_factory(prefix: string) {
        return function() {
            console.error(prefix, arguments)
        }
    }

    registerNewSagaRunner(function* watcherSaga() {
        yield takeEvery(actions.NEW_WARM_ERROR, logger_factory("WARNING"));
        yield takeEvery(actions.NEW_FATAL_ERROR, logger_factory("FATAL"));
    })
}

export { actions as default, warningErrorHappened, fatalErrorHappened };
