import { takeEvery, put, call } from "@redux-saga/core/effects";

import {getRegisteredActionTypes, registerNewAction, registerNewInitialState, registerNewSagaRunner} from "../redux-helper";
import {
    ReduxApiCachedActionCreator,
    ReduxApiCachedActionMap,
    ReduxApiCachedGeneratedState,
    ReduxApiCachedObjectWithId
} from "../types";
import errorManagerAction from "./error-manager"
import {apiCallGet} from "../fetch-from-api";
import {sortFactoryByField} from "../util/sorting-factory";
import deepLock, {DeepReadonly} from "deep-lock";


export function generateActionAndReducerForObject<ObjectType extends ReduxApiCachedObjectWithId>(moduleName: string, url: string, sortingFields: string[])
    : [ReduxApiCachedActionCreator<void>] {

    ////////////////////////////////////////////////////////////////////////////
    // Initial State
    ////////////////////////////////////////////////////////////////////////////

    registerNewInitialState(moduleName, {
        byId: {},
        order: sortingFields.reduce((result, key) => ({...result, [key]: []}), {})
    });

    ////////////////////////////////////////////////////////////////////////////
    // Actions and reducer
    ////////////////////////////////////////////////////////////////////////////

    const loadObjectActionCreator = registerNewAction<ReduxApiCachedGeneratedState<ObjectType>, void>(moduleName, moduleName.toUpperCase() + "_REQUESTED");

    registerNewAction<ReduxApiCachedGeneratedState<ObjectType>, ObjectType[]>(moduleName, moduleName.toUpperCase() + "_LOADED", (state, response) => {
        // @ts-ignore
        let byId: { [key: number|string]: ObjectType } = Object.assign({}, state.byId);

        for (let i = response.length; --i >= 0;) {
            byId[response[i].id] = response[i];
        }
        const order : { [key: string]: ObjectType["id"][] } = {};
        sortingFields.forEach((field) => {
            order[field] = response.sort(sortFactoryByField(field, "fr")).map(({id}) => id)
        });
        return {
            ...state,
            byId,
            order,
        };
    });

    const reduxActions = getRegisteredActionTypes(moduleName);

    ////////////////////////////////////////////////////////////////////////////
    // Saga
    ////////////////////////////////////////////////////////////////////////////

    function* loadObjectFromApi() : any {
        try {
            const response = yield call(apiCallGet, url);
            yield put({type: reduxActions[moduleName.toUpperCase() + "_LOADED"], payload: response});
        } catch (e) {
            yield put({type: errorManagerAction.NEW_WARM_ERROR, payload: e});
        }
    }

    registerNewSagaRunner(function* watcherSaga() {
        yield takeEvery(reduxActions[moduleName.toUpperCase() + "_REQUESTED"], loadObjectFromApi);
    })

    return [loadObjectActionCreator]
}
