import {
    createStore,
    applyMiddleware,
    compose,
    Store,
    AnyAction,
    combineReducers,
    ReducersMapObject, Middleware
} from "redux";
import createSagaMiddleware from "redux-saga";

import {ReduxApiCachedAction, ReduxApiCachedRootState} from "./types";
import {
    getAllRegisteredReducers, getAllRegisteredReducersForModule,
    getAllSagaRunner,
    getRegisteredInitialState
} from "./redux-helper";
import {register as registerErrorManager} from "./feature/error-manager";
import deepLock, {DeepReadonly} from "deep-lock";

const stores : { [key: string]: Store } = {};


function newReducer(state = getRegisteredInitialState(), action: ReduxApiCachedAction): DeepReadonly<ReduxApiCachedRootState> {
    const reducers = getAllRegisteredReducers();
    return reducers[action.type] === undefined ? state : deepLock({
        ...state,
        ...reducers[action.type](state, action.payload)
    });
}

function noop(state: any = null, action: any) {
    return state;
}

function updateOldReducer(oldReducers: ReducersMapObject) {

    const updatedReducer = {
        ...oldReducers
    };

    function generateReducer(key: string) {
        return (state = getRegisteredInitialState()[key], action: any) => {
            const reducer = getAllRegisteredReducersForModule(key)[action.type] || noop;
            return reducer(state, action.payload);
        }
    }

    Object.keys(getRegisteredInitialState()).forEach(key => {
        updatedReducer[key] = generateReducer(key);
    });

    const rootReducer = combineReducers(updatedReducer);

    return (state = getRegisteredInitialState(), action: AnyAction) => {
        return deepLock({
            ...state,
            ...rootReducer(state, action)
        });
    }
}


function createNewReduxStore(registerAllModule: Function, middlewares: Middleware[], oldReducers?: ReducersMapObject) {

    registerErrorManager();
    registerAllModule();

    const storeEnhancers = (window as any)['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] || compose;
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(
        oldReducers ? updateOldReducer(oldReducers): newReducer,
        storeEnhancers(
            applyMiddleware(sagaMiddleware, ...middlewares)
        )
    );

    const runners = getAllSagaRunner();

    for (let i = runners.length; --i >= 0;) {
        sagaMiddleware.run(runners[i]);
    }

    return store;
}


export function reduxStoreFactory(registerAllModule: Function, middlewares: Middleware[] = [], oldReducers?: ReducersMapObject) {

    if (stores[registerAllModule.name] === undefined) {
        stores[registerAllModule.name] = createNewReduxStore(registerAllModule, middlewares, oldReducers);
    }

    return stores[registerAllModule.name];
}
