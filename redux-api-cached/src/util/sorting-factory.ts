
export function sortFactoryByField(fieldName: string|number, locale: string) {

    return function compareObjByName(a: { [key: string|number]: any }, b: { [key: string|number]: any }) {
        const name_a = (a[fieldName] || "").toString();
        const name_b = (b[fieldName] || "").toString();

        return name_a.localeCompare(name_b, locale, {numeric: true});
    }
}