import axios, {AxiosInstance} from "axios";

// Set config defaults when creating the instance
let  axiosCustomInstance : AxiosInstance | null = null;

export function setAxios(axiosInstance : AxiosInstance) {
    axiosCustomInstance = axiosInstance;
}

function getAxios() {
    return axiosCustomInstance || axios;
}

class ApiError extends Error {

    path: string;
    response: Response;

    constructor(path: string, response: Response) {
        super(`Error (${response.status}) getting from API: ${path}`);
        this.path = path;
        this.response = response;
    }
}


export function apiCallGet(path: string) {
    console.log("GET", path);
    return getAxios()
        .get(path)
        .then(response => response.data)
        .catch(response => {
        throw new ApiError(path, response);
    });
}
