import {sortFactoryByField} from "../../src/util/sorting-factory";

const compareObjByName = sortFactoryByField("name", "fr")

test('compareObjByName (basic)', () => {
    const data = [
        {name: "hello", id: 99},
        {name: "héllo", id: 32},
        {name: "hallo", id: 54},
        {name: "hzllo", id: 83}
    ];

    const sortedData = data.sort(compareObjByName);

    expect(sortedData[0].name).toBe("hallo");
    expect(sortedData[0].id).toBe(54);
    expect(sortedData[1].name).toBe("hello");
    expect(sortedData[1].id).toBe(99);
    expect(sortedData[2].name).toBe("héllo");
    expect(sortedData[2].id).toBe(32);
    expect(sortedData[3].name).toBe("hzllo");
    expect(sortedData[3].id).toBe(83);
});


test('compareObjByName (with upper case)', () => {
    const data = [
        {name: "HZLLO", id: 82},
        {name: "hello", id: 99},
        {name: "héllo", id: 32},
        {name: "hallo", id: 54},
        {name: "hzllo", id: 83},
        {name: "HÉLLO", id: 31}
    ];

    const sortedData = data.sort(compareObjByName);

    expect(sortedData[0].name).toBe("hallo");
    expect(sortedData[0].id).toBe(54);
    expect(sortedData[1].name).toBe("hello");
    expect(sortedData[1].id).toBe(99);
    expect(sortedData[2].name).toBe("héllo");
    expect(sortedData[2].id).toBe(32);
    expect(sortedData[3].name).toBe("HÉLLO");
    expect(sortedData[3].id).toBe(31);
    expect(sortedData[4].name).toBe("hzllo");
    expect(sortedData[4].id).toBe(83);
    expect(sortedData[5].name).toBe("HZLLO");
    expect(sortedData[5].id).toBe(82);
});


test('compareObjByName (with number)', () => {
    const data = [
        {name: 41, id: 82},
        {name: "hello", id: 99},
        {name: 13, id: 27},
        {name: "009", id: 12},
        {name: "012", id: 76},
        {name: "14", id: 56},
        {name: 9, id: 98}
    ];

    const sortedData = data.sort(compareObjByName);

    expect(sortedData[0].name).toBe("009");
    expect(sortedData[1].name).toBe(9);
    expect(sortedData[2].name).toBe("012");
    expect(sortedData[3].name).toBe(13);
    expect(sortedData[4].name).toBe("14");
    expect(sortedData[5].name).toBe(41);
    expect(sortedData[6].name).toBe("hello");
});


test('compareObjByName (with undefined)', () => {
    const data = [
        {id: 99},
        {name: "hello", id: 32},
        {name: "hallo", id: 54}
    ];

    const sortedData = data.sort(compareObjByName);

    expect(sortedData[0].name).toBe(undefined);
    expect(sortedData[0].id).toBe(99);
    expect(sortedData[1].name).toBe("hallo");
    expect(sortedData[1].id).toBe(54);
    expect(sortedData[2].name).toBe("hello");
    expect(sortedData[2].id).toBe(32);
});


test('compareObjByName (with null)', () => {
    const data = [
        {name: null, id: 99},
        {name: "hello", id: 32},
        {name: "hallo", id: 54}
    ];

    const sortedData = data.sort(compareObjByName);

    expect(sortedData[0].name).toBe(null);
    expect(sortedData[0].id).toBe(99);
    expect(sortedData[1].name).toBe("hallo");
    expect(sortedData[1].id).toBe(54);
    expect(sortedData[2].name).toBe("hello");
    expect(sortedData[2].id).toBe(32);
});
