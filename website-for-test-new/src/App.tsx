import React from 'react';
import {Provider} from "react-redux";

import TodoList from "./feature/todo/component/TodoList";
import store from "./redux/store";

export default function App() {
    return <Provider store={store}>
        <div className="App">
            <TodoList />
        </div>
    </Provider>;
}
