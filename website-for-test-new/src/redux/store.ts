import {ReduxApiCachedRootState} from "redux-api-cached/types";
import {reduxStoreFactory} from "redux-api-cached";

import {register as registerModuleTodo, TodoReduxSubState} from "../feature/todo/redux";

function registerFeatures() {
    registerModuleTodo();
}

export interface ReduxRootState extends ReduxApiCachedRootState {
    todo: TodoReduxSubState;
}

const store = reduxStoreFactory(registerFeatures)

export default store